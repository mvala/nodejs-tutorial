var socket = io.connect();

var app = angular.module('MyApp', []);

app.controller("MyController", ["$scope", function($scope) {

    $scope.socket=socket;
    $scope.nUsers = 0;
    $scope.name = "Jozko";
    $scope.zmqmsg;

    //Listen to users_changed event
    $scope.socket.on('users_changed', function(data) {
        $scope.nUsers = data.nUsers;
        $scope.$digest();
    })
    $scope.socket.on('my_zmq_signal', function(data) {
      $scope.zmqmsg  = JSON.parse(data);
      $scope.$digest();
    })
}]);
