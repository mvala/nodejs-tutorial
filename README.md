# nodejs tutorial

## Install

### Add respoitory from
1. Go to https://copr.fedorainfracloud.org/coprs/mvala/hlit/ and download repo file. And save it in /etc/yum.repos.d

2. Install it via yum or dnf
```
yum install nodejs npm czmq-devel
```

## Start sensor (simple broker)
```
$ cd cpp-app
# compile app
$ gcc -lstdc++ -lczmq -lzmq -o test test.cpp
# Run app
$ ./test
```
## Start server

```
npm start
```

Visit web page at http://localhost:3000
