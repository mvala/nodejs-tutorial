#include <string>
#include <czmq.h>
using namespace std;
int main(int argc, char **argv) {
  int timeout = 1000;
  if (argc>1) timeout  = atoi(argv[1]);

  zsock_t* pub_snr = zsock_new_pub("@tcp://*:5000");
  assert(pub_snr);

  int i=0;
  int num;
  while (!zsys_interrupted) {
    num = rand() % 101;
    string json;
    json += "{ \"num\" : ";

    json += zsys_sprintf ("\"%d\"",num);  
    json += " }";

    zmsg_t* msg = zmsg_new();
    zmsg_addstr(msg, "");
    zmsg_addstrf(msg, "%s", json.data());
    printf("Sending %s\n", json.data());
    zmsg_send(&msg, pub_snr);
    zmsg_destroy(&msg);

    zclock_sleep(timeout);
    i++;
  }

  zsock_destroy(&pub_snr);
  return 0;
}
