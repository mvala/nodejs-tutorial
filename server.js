// Load express and socket.io.
var express = require('express.io'),
    app = express(),
    path = require('path')

var zmq = require('zmq'),
    sock = zmq.socket('sub');
var data_sub;
sock.connect('tcp://localhost:5000');
//sock.subscribe("");
console.log('Subscriber connected to port 5000');

app.http().io();
app.set('port', process.env.PORT || 3000);
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
    res.sendfile(__dirname + '/index.html');
});

var nUsers = 0;

app.io.on('connection', function(socket) {
    if (nUsers==0) {
        sock.subscribe("");
    }

    console.log('User ' + socket.id + ' connected');
    nUsers++;
    socket.emit("users_changed", {
        nUsers: nUsers
    })
    socket.broadcast.emit("users_changed", {
        nUsers: nUsers
    })

    socket.on('disconnect', function() {
	nUsers--;
	socket.emit("users_changed", {
		nUsers: nUsers
	})
	socket.broadcast.emit("users_changed", {
		nUsers: nUsers
	})
        console.log('User ' + socket.id + ' disconnected');
	if (nUsers==0) {
		sock.unsubscribe("");
        }
    });

    sock.on('message', function(topic, message) {
        console.log("topic=" + topic + " msg=" + message.toString());
        socket.emit("my_zmq_signal", message.toString());
    });
});

app.listen(app.get('port'), function() {
    console.log('HTTP server listening on port ' + app.get('port'));
});
